import socket
import sys

HOST = '10.0.0.17'
PORT = 65432

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
  sock.bind((HOST,PORT))
  print("Socket Successfully created")
  sock.listen()
  conn, addr = sock.accept()
  with conn:
    print('Connected by', addr)
    while True:
      data = conn.recv(1024)
      print('received: ',data)
      if data:
        sock.close()
        break
