#!/usr/bin/python
# -*- coding:utf-8 -*-
import random
import glob
import API.CFGAPI as CFG
import API.DFAAPI as DFA
import API.converter as converter
from MeasurementModule.policy_generation import PolicyGeneration, NUMBER_SOURCE_TARGET
from checker import Checker

# construct DFA based Data Extractor
consumer = DFA.dfa_construction('DataModel/cfi_dm.txt')
consumer_dfa = consumer[0]
consumer_extractedinfo = consumer[1]

# construct CFG based Policy Generator
nsf_facing = CFG.cfg_construction('DataModel/nfi_dm.txt')
cfglist = nsf_facing[0]
nsf_requiredinfo = nsf_facing[1]

# construct Data Converter
dataconverter = converter.DataConverter(consumer_extractedinfo, nsf_requiredinfo)
dataconverter.initializeDB()

# construct conflicting checker
checker = Checker([], consumer_dfa, consumer_extractedinfo, is_measure=True)


def deploy_intent(policy_file_name):
    # extract data
    consumer_extractedlist = DFA.extracting_data(policy_file_name, consumer_dfa, consumer_extractedinfo)
    dataconverter.inputExtractedData(consumer_extractedlist)

    # check conflict ----------------------------------
    checker.checkConflict(consumer_extractedlist)
    # -------------------------------------------------

    # save new rule extracted
    checker.insert_new_rule(dataconverter)
    # -------------------------------------------------

    # convert data & provisioning
    print('Convert data...')
    dataconverter.convertData()
    print('Policy provisioning...')


if __name__ == "__main__":
    # randomly generate rule set ----------------------
    rule_generation_instance = PolicyGeneration()
    is_generate_new_set = True
    if is_generate_new_set:
        conflict_rules, never_conflicting_rules, invalid_partial_ordering, size_rule, size_explicit, size_implicit, size_pass, size_drop = \
            rule_generation_instance.initialize_high_level_policy_DB(rule_number=100, is_contain_implicit=True,
                                                                     partial_ordering_number=10, max_num_conflict=1000,
                                                                     percent_explicit=0.5, conflict_percentage=1)
    rule_generation_instance.add_converted_data()
    # -------------------------------------------------

    # deploy separation constraint --------------------
    is_delpoy_separation_constraint = True
    invalid_constraint = []
    if is_delpoy_separation_constraint:
        file_separation = sorted(glob.glob("HighLevelPolicy/Usecase/separation*.txt"))
        for file in file_separation:
            rs = checker.SC_API.deploy_separation_constraint(file)
            if rs:
                invalid_constraint.append(rs)
    # -------------------------------------------------

    # deploy partial ordering relationship ------------
    is_delpoy_order_relationship = False
    invalid_ordering = []
    if is_delpoy_order_relationship:
        file_ordering = sorted(glob.glob("HighLevelPolicy/Usecase/ordering*.txt"))
        for file in file_ordering:
            rs = checker.PO_API.deploy_partial_ordering_relationship(file)
            if rs:
                invalid_ordering.append(rs)
    # -------------------------------------------------

    # deploy rules ------------------------------------
    file_rule = sorted(glob.glob("HighLevelPolicy/Usecase/R*.txt"))
    random.shuffle(file_rule)
    k = 0
    for rule in file_rule:
        deploy_intent(rule)
        print(str(k) + "th deployed")
        k += 1
    # -------------------------------------------------

    print "Total conflict after applying SC: ", len(checker.total_conflicting_rules), "\n", checker.total_conflicting_rules
