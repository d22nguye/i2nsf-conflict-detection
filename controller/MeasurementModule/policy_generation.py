import pandas as pd
import random, os
from pandas.compat import FileNotFoundError
import module
import ipaddress, struct, MySQLdb

NUMBER_SOURCE_TARGET = 300

TIME_SLOT = [i.strftime("%H:%M") for i in pd.date_range("00:00", "23:59", freq="2min").time]
ARRAY_TIME_SLOT = [[TIME_SLOT[i], TIME_SLOT[i + 1]] for i in range(0, len(TIME_SLOT) - 1, 2)]

SOURCE_TARGET = ["src-" + str(i) for i in range(NUMBER_SOURCE_TARGET)]

DESTINATION_TARGET = [("web-" + str(i)) for i in range(15000)]
CONVERTED_DESTINATION_TARGET = [("url-" + str(i)) for i in range(len(DESTINATION_TARGET) * 2 / 3 + 1000)]

ACTION = ["pass", "drop"]

max_number_of_time_range = len(ARRAY_TIME_SLOT) / 3
max_number_of_src_range = len(SOURCE_TARGET) / 3
max_number_of_dst_range = len(DESTINATION_TARGET) / 3
RANGE_OF_TIME = 5

xmlTemplate = """<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <rule>
    <rule-name>R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-%(dest-target)s-%(action)s</rule-name>
    <event>
      <time-information>
        <begin-time>%(begin-time)s</begin-time>
        <end-time>%(end-time)s</end-time>
      </time-information>
    </event>
    <condition>
      <firewall-condition>
        <source-target>
          <src-target>%(src-target)s</src-target>
        </source-target>
      </firewall-condition>
      <custom-condition>
        <destination-target>
          <dest-target>%(dest-target)s</dest-target>
        </destination-target>
      </custom-condition>
    </condition>
    <action>
      <primary-action>%(action)s</primary-action>
    </action>
  </rule>
</ietf-i2nsf-cfi-policy:policy>
"""

xmlTemplateNoEvent = """<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <rule>
    <rule-name>R%(index)s-AnyTime-%(src-target)s-%(dest-target)s-%(action)s</rule-name>
    <condition>
      <firewall-condition>
        <source-target>
          <src-target>%(src-target)s</src-target>
        </source-target>
      </firewall-condition>
      <custom-condition>
        <destination-target>
          <dest-target>%(dest-target)s</dest-target>
        </destination-target>
      </custom-condition>
    </condition>
    <action>
      <primary-action>%(action)s</primary-action>
    </action>
  </rule>
</ietf-i2nsf-cfi-policy:policy>
"""

xmlTemplateNoSrc = """<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <rule>
    <rule-name>R%(index)s-%(begin-time)s-%(end-time)s-AnySrc-%(dest-target)s-%(action)s</rule-name>
    <event>
      <time-information>
        <begin-time>%(begin-time)s</begin-time>
        <end-time>%(end-time)s</end-time>
      </time-information>
    </event>
    <condition>
      <custom-condition>
        <destination-target>
          <dest-target>%(dest-target)s</dest-target>
        </destination-target>
      </custom-condition>
    </condition>
    <action>
      <primary-action>%(action)s</primary-action>
    </action>
  </rule>
</ietf-i2nsf-cfi-policy:policy>
"""

xmlTemplateNoDst = """<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <rule>
    <rule-name>R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-AnyDst-%(action)s</rule-name>
    <event>
      <time-information>
        <begin-time>%(begin-time)s</begin-time>
        <end-time>%(end-time)s</end-time>
      </time-information>
    </event>
    <condition>
      <firewall-condition>
        <source-target>
          <src-target>%(src-target)s</src-target>
        </source-target>
      </firewall-condition>
    </condition>
    <action>
      <primary-action>%(action)s</primary-action>
    </action>
  </rule>
</ietf-i2nsf-cfi-policy:policy>
"""

xmlTemplateSeparation = """
<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <separation-constraint>
    <not-share-common-value>
        <attribute-id>%(att-id)s</attribute-id>
        <value>%(value-1)s</value>
        <value>%(value-2)s</value>
    </not-share-common-value>
  </separation-constraint>
</ietf-i2nsf-cfi-policy:policy>
"""

partial_ordering_relationship_template = """
<ietf-i2nsf-cfi-policy:policy>
  <policy-name>security_policy_for_sns</policy-name>
  <priority-order-rules>
    <rule-identity>%(R1)s</rule-identity>
    <rule-identity>%(R2)s</rule-identity>
  </priority-order-rules>
</ietf-i2nsf-cfi-policy:policy>
"""


# src-target


def pick_time(i):
    global ARRAY_TIME_SLOT
    return ARRAY_TIME_SLOT[i]


def pick_src(i):
    global SOURCE_TARGET
    return SOURCE_TARGET[i]


def pick_dst(i):
    global DESTINATION_TARGET
    return DESTINATION_TARGET[i]


def random_ip(network):
    network = ipaddress.IPv4Network(network)
    network_int, = struct.unpack("!I", network.network_address.packed)  # make network address into an integer
    rand_bits = network.max_prefixlen - network.prefixlen  # calculate the needed bits for the host part
    rand_host_int = random.randint(0, 2 ** rand_bits - 1)  # generate random host part
    ip_address = ipaddress.IPv4Address(network_int + rand_host_int)  # combine the parts
    return str(ip_address.exploded)


def random_ip_address(u_ip_prefix):
    network = ipaddress.IPv4Network(u_ip_prefix)

    start_ip = random_ip(u_ip_prefix)
    while (start_ip == network[-1]) | (start_ip == network[0]) | (start_ip == [-2]):
        start_ip = random_ip(u_ip_prefix)

    end_ip = random_ip(u_ip_prefix)
    while (end_ip == network[-1]) | (end_ip == network[0]) | module.is_less_ip(end_ip, start_ip):
        end_ip = random_ip(u_ip_prefix)
    return start_ip, end_ip


# def generate_separation_constraint(separation_number, is_random):
#     global SOURCE_TARGET, max_number_of_src_range
#     index = 0
#     size_index = len(str((max_number_of_src_range - 1) * max_number_of_src_range / 2))
#     for i in range(max_number_of_src_range):
#         src_target_1 = SOURCE_TARGET[i]
#         for j in range(i + 1, max_number_of_src_range):
#             if index >= separation_number:
#                 return index
#             src_target_2 = SOURCE_TARGET[j]
#             if (not is_random) | random.randint(0, 1):
#                 data = {'index': str(index).zfill(size_index), 'att-id': 'src-target', 'value-1': src_target_1,
#                         'value-2': src_target_2}
#                 content = xmlTemplateSeparation % data
#                 rule_name = "separation-%(index)s-%(value-1)s-%(value-2)s.txt" % data
#                 f = open("HighLevelPolicy/Usecase/" + rule_name, "w")
#                 f.write(content)
#                 f.close()
#                 index += 1
#     return index

class PolicyGeneration:
    def __init__(self):
        self.index_separation = 0
        self.map_separation_constraint_generated = {}
        self.index_partial_ordering = 0

    def generate_separation_constraint_by_target(self, src_target_1, src_target_2, separation_number):
        if ((src_target_1 + src_target_2) not in self.map_separation_constraint_generated) | (
                (src_target_2 + src_target_1) not in self.map_separation_constraint_generated):
            self.map_separation_constraint_generated[src_target_1 + src_target_2] = 1
            self.map_separation_constraint_generated[src_target_2 + src_target_1] = 1
        else:
            return
        size_index = len(str(separation_number))
        if self.index_separation >= separation_number:
            return self.index_separation
        data = {'index': str(self.index_separation).zfill(size_index), 'att-id': 'src-target', 'value-1': src_target_1,
                'value-2': src_target_2}
        content = xmlTemplateSeparation % data
        rule_name = "separation-%(index)s-%(value-1)s-%(value-2)s.txt" % data
        f = open("HighLevelPolicy/Usecase/" + rule_name, "w")
        f.write(content)
        f.close()
        self.index_separation += 1
        return self.index_separation

    def is_overlap_ip_address(self, src_target_1, src_target_2, data_src_target):
        start_time_1, end_time_1 = data_src_target[src_target_1]
        start_time_2, end_time_2 = data_src_target[src_target_2]
        if not (module.is_less_ip(end_time_1, start_time_2) | module.is_less_ip(end_time_2, start_time_1)):
            return True
        return False

    def generate_converted_data(self):
        global SOURCE_TARGET, DESTINATION_TARGET, CONVERTED_DESTINATION_TARGET

        src_target_insert_cmd = []
        dst_target_insert_cmd = []

        src_target_delete_cmd = []
        dst_target_delete_cmd = []
        data_src_target = {}
        data_dst_target = {}

        for i in range(len(SOURCE_TARGET)):
            src_target = SOURCE_TARGET[i]
            start_ip, end_ip = random_ip_address(u'103.112.2.0/24')
            src_target_insert_cmd.append(
                "INSERT INTO endpointtable (ename, id, data) VALUES ('" + src_target + "', 43, '" + start_ip + "')")
            src_target_insert_cmd.append(
                "INSERT INTO endpointtable (ename, id, data) VALUES ('" + src_target + "', 44, '" + end_ip + "')")
            src_target_delete_cmd.append("DELETE FROM endpointtable where ename='" + src_target + "'")
            data_src_target[src_target] = [start_ip, end_ip]

        for i in range(len(DESTINATION_TARGET)):
            dst_target = DESTINATION_TARGET[i]
            url_number = random.randint(1, 5)
            converted_data = ','.join(random.sample(CONVERTED_DESTINATION_TARGET, url_number))

            dst_target_insert_cmd.append(
                "INSERT INTO endpointtable (ename, id, data) VALUES ('" + dst_target + "', 114, '" + converted_data + "')")
            dst_target_insert_cmd.append(
                "INSERT INTO endpointtable (ename, id, data) VALUES ('" + dst_target + "', 123, 'url-filtering')")
            dst_target_delete_cmd.append("DELETE FROM endpointtable where ename='" + dst_target + "'")
            data_dst_target[dst_target] = converted_data
        return src_target_insert_cmd, dst_target_insert_cmd, src_target_delete_cmd, dst_target_delete_cmd, data_src_target, data_dst_target

    def create_rule(self, index, size_index, start_time, end_time, src_target, dst_target, action, template_name,
                    xmlTemplate):
        global ACTION
        data = {'index': str(index).zfill(size_index), 'begin-time': start_time, 'end-time': end_time,
                'src-target': src_target,
                'dest-target': dst_target, 'action': action}
        content = xmlTemplate % data
        rule_name = template_name % data
        f = open("HighLevelPolicy/Usecase/" + rule_name, "w")
        f.write(content)
        f.close()
        return rule_name

    def generate_partial_ordering_relationship(self, name_rule_1, name_rule_2, partial_ordering_number):
        size_index = len(str(partial_ordering_number))
        if self.index_partial_ordering >= partial_ordering_number:
            return self.index_partial_ordering

        data = {'index': str(self.index_partial_ordering).zfill(size_index), 'R1': name_rule_1, 'R2': name_rule_2}
        content = partial_ordering_relationship_template % data
        rule_name = "ordering-%(index)s-%(R1)s-%(R2)s.txt" % data
        f = open("HighLevelPolicy/Usecase/" + rule_name, "w")
        f.write(content)
        f.close()
        self.index_partial_ordering += 1

    def initialize_high_level_policy_DB(self, rule_number, is_contain_implicit, partial_ordering_number,
                                        max_num_conflict, percent_explicit, conflict_percentage):
        global max_number_of_time_range, max_number_of_src_range, max_number_of_dst_range

        DIR = module.get_dir_project()
        usecase_dir = DIR + '/controller/HighLevelPolicy/Usecase'
        if not os.path.exists(usecase_dir):
            os.makedirs(usecase_dir)

        self.map_separation_constraint_generated = {}
        self.index_separation = 0
        self.index_partial_ordering = 0
        number_conflict_need = conflict_percentage * 1.0 * (rule_number * (rule_number - 2)) / 2 / 100

        separation_number = (NUMBER_SOURCE_TARGET * (NUMBER_SOURCE_TARGET - 2) / 2) * 0.01
        # separation_number = (NUMBER_SOURCE_TARGET * (NUMBER_SOURCE_TARGET - 2) / 2)
        self.delete_converted_data()
        save_info_generation = ""
        os.system('rm -rf HighLevelPolicy/Usecase/*')

        src_target_insert_cmd, dst_target_insert_cmd, src_target_delete_cmd, dst_target_delete_cmd, data_src_target, data_dst_target = self.generate_converted_data()
        with open('HighLevelPolicy/Usecase/src_target_insert_cmd.txt', 'w') as filehandle:
            for list_item in src_target_insert_cmd:
                filehandle.write('%s\n' % list_item)
        with open('HighLevelPolicy/Usecase/dst_target_insert_cmd.txt', 'w') as filehandle:
            for list_item in dst_target_insert_cmd:
                filehandle.write('%s\n' % list_item)
        with open('HighLevelPolicy/Usecase/src_target_delete_cmd.txt', 'w') as filehandle:
            for list_item in src_target_delete_cmd:
                filehandle.write('%s\n' % list_item)
        with open('HighLevelPolicy/Usecase/dst_target_delete_cmd.txt', 'w') as filehandle:
            for list_item in dst_target_delete_cmd:
                filehandle.write('%s\n' % list_item)

        # separation_number = generate_separation_constraint(separation_number, is_random_separation)
        # separation_number = generate_separation_constraint(100, 10)

        conflict_rules = []
        never_conflicting_rules = []
        invalid_partial_ordering = []

        index = 0
        size_index = len(str(rule_number))
        max_number_of_time_range = len(ARRAY_TIME_SLOT) / RANGE_OF_TIME
        max_number_of_src_range = len(SOURCE_TARGET) / max_number_of_time_range

        # separate time
        size_implicit = 0
        size_explicit = 0

        size_pass = 0
        size_drop = 0

        switch_action = 0
        for index_time in range(0, len(ARRAY_TIME_SLOT), RANGE_OF_TIME):
            time = pick_time(index_time + RANGE_OF_TIME / 2)
            start_time = time[0]
            end_time = time[1]

            src_target_base = pick_src(random.randint(0, len(SOURCE_TARGET) - 1))
            dst_target = pick_dst(random.randint(0, len(DESTINATION_TARGET) - 1))

            if number_conflict_need > len(conflict_rules):
                is_need_conflict = True
                if switch_action:  # random.randint(0, 1):
                    size_drop += 1
                    action_pick = 1
                    switch_action = 0
                else:
                    size_pass += 1
                    action_pick = 0
                    switch_action = 1

                rule_name = self.create_rule(index, size_index, start_time, end_time, src_target_base, dst_target,
                                             ACTION[action_pick],
                                             "R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-%(dest-target)s-%(action)s.txt",
                                             xmlTemplate)
                save_name = [[rule_name.replace(".txt", "")]]
                index += 1
                if index >= rule_number:
                    break
            else:
                if switch_action:  # random.randint(0, 1):
                    action_pick = 1
                    switch_action = 0
                else:
                    action_pick = 0
                    switch_action = 1
            # 10000 -> 2000
            # 1000 -> 200
            if rule_number > 9:
                number_of_opposite_rule = random.randint(0.2 * rule_number, 0.3 * rule_number)
            else:
                number_of_opposite_rule = rule_number / 2

            for i in range(number_of_opposite_rule):
                if len(conflict_rules) >= max_num_conflict:
                    break
                src_target = pick_src(random.randint(0, len(SOURCE_TARGET) - 1))
                dst_target = pick_dst(random.randint(0, len(DESTINATION_TARGET) - 1))

                action_opposite = 1 - action_pick
                if (number_conflict_need > len(conflict_rules)) & (i < (number_conflict_need / 72)):
                    start_time = pick_time(random.randint(index_time, index_time + RANGE_OF_TIME / 2 - 1))[
                        random.randint(0, 1)]
                    end_time = pick_time(
                        random.randint(index_time + RANGE_OF_TIME / 2 + 1, index_time + RANGE_OF_TIME / 2 + 2))[
                        random.randint(0, 1)]

                    if random.random() < percent_explicit:
                        rule_name = self.create_rule(index, size_index, start_time, end_time, src_target, dst_target,
                                                     ACTION[action_opposite],
                                                     "R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-%(dest-target)s-%(action)s.txt",
                                                     xmlTemplate)
                        size_explicit += 1
                        if action_opposite:
                            size_drop += 1
                        else:
                            size_pass += 1
                        save_name_temp = save_name[:]
                        save_name_temp.append([rule_name.replace(".txt", "")])
                        conflict_rules.append(save_name_temp)
                        if not self.is_overlap_ip_address(src_target_base, src_target, data_src_target):
                            never_conflicting_rules.append(save_name_temp)
                            self.generate_separation_constraint_by_target(src_target_base, src_target,
                                                                          separation_number)

                        self.generate_partial_ordering_relationship(save_name_temp[0][0], save_name_temp[1][0],
                                                                    partial_ordering_number)
                        if (self.index_partial_ordering < partial_ordering_number) and random.randint(0,
                                                                                                      1):  # create invalid partial ordering relationship or not
                            self.generate_partial_ordering_relationship(save_name_temp[1][0], save_name_temp[0][0],
                                                                        partial_ordering_number)
                            invalid_partial_ordering.append([save_name_temp[1][0], save_name_temp[0][0]])

                        index += 1
                        if index >= rule_number:
                            break
                    if is_contain_implicit:
                        if random.random() < ((1 - percent_explicit) / 2.0):
                            rule_name = self.create_rule(index, size_index, start_time, end_time, src_target,
                                                         dst_target,
                                                         ACTION[action_opposite],
                                                         "R%(index)s-%(begin-time)s-%(end-time)s-AnySrc-%(dest-target)s-%(action)s.txt",
                                                         xmlTemplateNoSrc)
                            size_implicit += 1
                            if action_opposite:
                                size_drop += 1
                            else:
                                size_pass += 1
                            save_name_temp = save_name[:]
                            save_name_temp.append([rule_name.replace(".txt", "")])
                            conflict_rules.append(save_name_temp)

                            self.generate_partial_ordering_relationship(save_name_temp[0][0], save_name_temp[1][0],
                                                                        partial_ordering_number)
                            if (self.index_partial_ordering < partial_ordering_number) and random.randint(0,
                                                                                                          1):  # create invalid partial ordering relationship or not
                                self.generate_partial_ordering_relationship(save_name_temp[1][0], save_name_temp[0][0],
                                                                            partial_ordering_number)
                                invalid_partial_ordering.append([save_name_temp[1][0], save_name_temp[0][0]])

                            index += 1
                            if index >= rule_number:
                                break
                        if random.random() < ((1 - percent_explicit) / 2.0):
                            rule_name = self.create_rule(index, size_index, start_time, end_time, src_target,
                                                         dst_target,
                                                         ACTION[action_opposite],
                                                         "R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-AnyDst-%(action)s.txt",
                                                         xmlTemplateNoDst)
                            size_implicit += 1
                            if action_opposite:
                                size_drop += 1
                            else:
                                size_pass += 1
                            save_name_temp = save_name[:]
                            save_name_temp.append([rule_name.replace(".txt", "")])
                            conflict_rules.append(save_name_temp)

                            if not self.is_overlap_ip_address(src_target_base, src_target, data_src_target):
                                never_conflicting_rules.append(save_name_temp)
                                self.generate_separation_constraint_by_target(src_target_base, src_target,
                                                                              separation_number)

                            self.generate_partial_ordering_relationship(save_name_temp[0][0], save_name_temp[1][0],
                                                                        partial_ordering_number)
                            if (self.index_partial_ordering < partial_ordering_number) and random.randint(0,
                                                                                                          1):  # create invalid partial ordering relationship or not
                                self.generate_partial_ordering_relationship(save_name_temp[1][0], save_name_temp[0][0],
                                                                            partial_ordering_number)
                                invalid_partial_ordering.append([save_name_temp[1][0], save_name_temp[0][0]])

                            index += 1
                            if index >= rule_number:
                                break
                else:
                    if random.randint(0, 1):
                        start_time, end_time = pick_time(random.randint(index_time, index_time + RANGE_OF_TIME / 2 - 1))
                    else:
                        start_time, end_time = pick_time(
                            random.randint(index_time + RANGE_OF_TIME / 2 + 1, index_time + RANGE_OF_TIME / 2 + 2))
                    self.create_rule(index, size_index, start_time, end_time, src_target, dst_target,
                                     ACTION[action_opposite],
                                     "R%(index)s-%(begin-time)s-%(end-time)s-%(src-target)s-%(dest-target)s-%(action)s.txt",
                                     xmlTemplate)
                    if action_opposite:
                        size_drop += 1
                    else:
                        size_pass += 1
                    index += 1
                    if index >= rule_number:
                        break
            if index >= rule_number:
                break

        # for m in range(len(SOURCE_TARGET)):
        #     for n in range(m + 1, len(SOURCE_TARGET)):
        #         src_target = SOURCE_TARGET[n]
        #         src = SOURCE_TARGET[m]
        #         if not self.is_overlap_ip_address(src, src_target, data_src_target):
        #             self.generate_separation_constraint_by_target(src, src_target, separation_number)
        #         if self.index_separation >= separation_number:
        #             break
        index_time += 1
        print "total conflict: ", len(conflict_rules), " - indext_time: ", index_time

        save_info_generation += "rule size: " + str(index) + "\nimplicit: " + str(size_implicit) + "\nexplicit: " + str(
            size_explicit) + "\ntotal conflict: " + str(len(conflict_rules)) + "\ntotal conflict after SC deployment: " + str(
            len(conflict_rules) - len(never_conflicting_rules)) + "\nmax number of separation constraint: " + str(
            self.index_separation) + "\ninvalid partial ordering number: " + str(
            len(invalid_partial_ordering)) + "\npass size: " + str(size_pass) + "\ndrop size: " + str(
            size_drop) + "\n" + str(index) + " & " + str(self.index_separation) + " & " + str(
            len(conflict_rules)) + " & " + str(len(conflict_rules) - len(never_conflicting_rules)) + " & " + str(
            size_explicit) + " & " + str(size_implicit) + " & " + str(size_pass) + " & " + str(size_drop)
        # str(is_contain_implicit)
        f = open("MeasurementModule/Evaluation/save_info_generation.txt", "w")
        f.write(save_info_generation)
        f.close()

        return conflict_rules, never_conflicting_rules, invalid_partial_ordering, index, size_explicit, size_implicit, size_pass, size_drop

        # return src_target_insert_cmd, dst_target_insert_cmd

    def add_converted_data(self):
        src_target_insert_cmd = []
        dst_target_insert_cmd = []

        try:
            with open('HighLevelPolicy/Usecase/src_target_insert_cmd.txt', 'r') as filehandle:
                for line in filehandle:
                    current_place = line[:-1]
                    src_target_insert_cmd.append(current_place)
        except FileNotFoundError:
            print("File src_target_insert_cmd.txt not exist")

        try:
            with open('HighLevelPolicy/Usecase/dst_target_insert_cmd.txt', 'r') as filehandle:
                for line in filehandle:
                    current_place = line[:-1]
                    dst_target_insert_cmd.append(current_place)
        except FileNotFoundError:
            print("File dst_target_insert_cmd.txt not exist")

        nsfdb = MySQLdb.connect(host="localhost", user="root", passwd="secu", db="nsfdb")
        nsfcur = nsfdb.cursor()

        for cmd in src_target_insert_cmd:
            nsfcur.execute(cmd)

        for cmd in dst_target_insert_cmd:
            nsfcur.execute(cmd)

        nsfdb.commit()
        nsfcur.close()

    def delete_converted_data(self):
        # path_delete_src = 'HighLevelPolicy/Usecase/src_target_delete_cmd.txt'
        # path_delete_dst = 'HighLevelPolicy/Usecase/dst_target_delete_cmd.txt'
        # if (not os.path.exists(path_delete_src)) | (not os.path.exists(path_delete_dst)):
        #     return

        # src_target_delete_cmd = []
        # dst_target_delete_cmd = []
        #
        # with open(path_delete_src, 'r') as filehandle:
        #     for line in filehandle:
        #         current_place = line[:-1]
        #         src_target_delete_cmd.append(current_place)
        #
        # with open(path_delete_dst, 'r') as filehandle:
        #     for line in filehandle:
        #         current_place = line[:-1]
        #         dst_target_delete_cmd.append(current_place)
        #
        nsfdb = MySQLdb.connect(host="localhost", user="root", passwd="secu", db="nsfdb")
        nsfcur = nsfdb.cursor()
        #
        # for cmd in src_target_delete_cmd:
        #     nsfcur.execute(cmd)
        #
        # for cmd in dst_target_delete_cmd:
        #     nsfcur.execute(cmd)

        cmd = "DELETE FROM endpointtable"
        nsfcur.execute(cmd)
        nsfdb.commit()
        nsfcur.close()

# if __name__ == "__main__":
#     initialize_high_level_policy_DB(1000)
#     print(pick_time(1))
