list_basic_detection = [[['block_access_to_sns_during_office_hours'], ['block_sns_8_9_pass']],
                        [['block_access_to_sns_during_office_hours'], ['block_sns_10_11_pass']],
                        [['block_access_to_sns_during_office_hours'], ['block_sns_17_1930_pass']],
                        [['block_access_to_sns_during_office_hours'], ['block_sns_HR_pass']],
                        [['block__sns_8_830_pass'], ['block_sns_HR_missing_time_drop']],
                        [['block_sns_8_9_pass'], ['block_sns_HR_missing_time_drop']],
                        [['block_sns_10_11_pass'], ['block_sns_HR_missing_time_drop']],
                        [['block_sns_19_20_pass'], ['block_sns_HR_same_decision_drop']],
                        [['block_sns_19_20_pass'], ['block_sns_HR_missing_time_drop']],
                        [['block_sns_17_1930_pass'], ['block_sns_HR_same_decision_drop']],
                        [['block_sns_17_1930_pass'], ['block_sns_HR_missing_time_drop']],
                        [['block_sns_HR_pass'], ['block_sns_HR_same_decision_drop']],
                        [['block_sns_HR_pass'], ['block_sns_HR_missing_time_drop']]]
list_binarysearch_detection = [[['block_access_to_sns_during_office_hours'], ['block_sns_8_9_pass']],
                               [['block_access_to_sns_during_office_hours'], ['block_sns_10_11_pass']],
                               [['block_access_to_sns_during_office_hours'], ['block_sns_17_1930_pass']],
                               [['block_access_to_sns_during_office_hours'], ['block_sns_HR_pass']],
                               [['block_sns_HR_pass'], ['block_sns_HR_same_decision_drop']],
                               [['block_sns_17_1930_pass'], ['block_sns_HR_same_decision_drop']],
                               [['block_sns_19_20_pass'], ['block_sns_HR_same_decision_drop']],
                               [['block_sns_10_11_pass'], ['block_sns_HR_missing_time_drop']],
                               [['block_sns_8_9_pass'], ['block_sns_HR_missing_time_drop']],
                               [['block__sns_8_830_pass'], ['block_sns_HR_missing_time_drop']],
                               [['block_sns_HR_pass'], ['block_sns_HR_missing_time_drop']],
                               [['block_sns_17_1930_pass'], ['block_sns_HR_missing_time_drop']],
                               [['block_sns_19_20_pass'], ['block_sns_HR_missing_time_drop']]]


def check_same_contents(nums1, nums2):
    isSame = True
    map = {}
    for element in nums1:
        map[element[0][0] + "-" + element[1][0]] = element
        map[element[1][0] + "-" + element[0][0]] = element

    for element in nums2:
        isExist = True
        key = element[0][0] + "-" + element[1][0]
        if key in map:
            map.pop(key)
        else:
            isExist = False
        key = element[1][0] + "-" + element[0][0]
        if key in map:
            map.pop(key)
        else:
            isExist = False

        if not isExist:
            print (element, "in the second set")
            isSame = False

    check = {}
    for key in map.keys():
        if (map[key][0][0] + map[key][1][0]) not in check:
            check[map[key][0][0] + map[key][1][0]] = 1
            print (map[key], "in the first set")
            isSame = False
    return isSame


def check_same_contents_with_constraint_list(nums1, nums2, never_conflicting_rules):
    isSame = True
    map = {}
    for element in nums1:
        map[element[0][0] + "-" + element[1][0]] = element
        map[element[1][0] + "-" + element[0][0]] = element

    map_constraint = {}
    for element in never_conflicting_rules:
        map_constraint[element[0][0] + "-" + element[1][0]] = element
        map_constraint[element[1][0] + "-" + element[0][0]] = element

    for element in nums2:
        isExist = True
        key = element[0][0] + "-" + element[1][0]
        if key in map:
            map.pop(key)
        else:
            isExist = False
        key = element[1][0] + "-" + element[0][0]
        if key in map:
            map.pop(key)
        else:
            isExist = False

        if not isExist:
            isExist = True
            key = element[0][0] + "-" + element[1][0]
            if key in map_constraint:
                map_constraint.pop(key)
            else:
                isExist = False
            key = element[1][0] + "-" + element[0][0]
            if key in map_constraint:
                map_constraint.pop(key)
            else:
                isExist = False
            if not isExist:
                print (element, "in the second set")
                isSame = False

    check = {}
    for key in map.keys():
        if (map[key][0][0] + map[key][1][0]) not in check:
            check[map[key][0][0] + map[key][1][0]] = 1
            print (map[key], "in the first set")
            isSame = False
    return isSame

def check_accuracy_partial_ordering_detect(invalid_partial_ordering, invalid_ordering):
    is_different = False
    for element in invalid_ordering:
        if element not in invalid_partial_ordering:
            print (element, "in invalid_ordering")
            is_different = True
    return is_different
# check_same_contents(list_basic_detection, list_binarysearch_detection)
