import psutil
import threading
import os

max_RAM = 0
end_process = 0
process = psutil.Process(os.getpid())

import time

list_measure = []
st = 0
et = 0


def validate_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


def split_ip(ip):
    return tuple(int(part) for part in ip.split('.'))


def get_key_ip(item):
    return split_ip(item)


def is_less_ip(ip1, ip2):
    split_ip_1 = split_ip(ip1)
    split_ip_2 = split_ip(ip2)
    for i in range(4):
        if split_ip_1[i] != split_ip_2[i]:
            return split_ip_1[i] < split_ip_2[i]
    return False


def is_different_decision_extracted(rule_1, rule_2):
    return ((rule_1[20][0] == "drop") & (rule_2[20][0] == "pass")) | (
            (rule_1[20][0] == "pass") & (rule_2[20][0] == "drop"))


def is_different_decision(rule_1, rule_2):
    return ((rule_1[120][0] == "drop") & (rule_2[120][0] == "pass")) | (
            (rule_1[120][0] == "pass") & (rule_2[120][0] == "drop")) | (
                   (rule_1[121][0] == "drop") & (rule_2[121][0] == "pass")) | (
                   (rule_1[121][0] == "pass") & (rule_2[121][0] == "drop"))


def start_measure_clock():
    global st
    st = time.time()


def end_measure_clock():
    global et, st
    et = time.time()
    elapsed_time = et - st
    list_measure.append(elapsed_time)
    print('Execution time:', elapsed_time, 'seconds - average: ', sum(list_measure) / len(list_measure), " - size:",
          len(list_measure))


def get_dir_project():
    DIR = os.path.abspath(os.curdir)
    if "i2nsf-conflict-detection" in DIR:
        while DIR.split("/")[-1] != 'i2nsf-conflict-detection':
            DIR = os.path.abspath(os.path.join(DIR, os.pardir))
    return DIR


def measure_ram():
    p = threading.Timer(0.5, measure_ram)
    p.start()
    global max_RAM, process
    temp_RAM = process.memory_info().rss / 1024.0 ** 2
    max_RAM = max(max_RAM, temp_RAM)

    DIR = get_dir_project()
    with open(DIR + '/controller/HighLevelPolicy/Usecase/ram_temp.txt', 'a') as filehandle:
        filehandle.write('%s\n' % (str(temp_RAM)))
    with open(DIR + '/controller/HighLevelPolicy/Usecase/max_ram_temp.txt', 'w') as filehandle:
        filehandle.write('%s\n' % (str(max_RAM)))
    # print (p.getName(), max_RAM, temp_RAM)
    if end_process == 1:
        max_RAM = 0
        temp_RAM = 0
        p.cancel()
        return


def setup_ram_mesurement():
    DIR = get_dir_project()

    usecase_dir = DIR + '/controller/HighLevelPolicy/Usecase'
    if not os.path.exists(usecase_dir):
        os.makedirs(usecase_dir)

    usecase_dir = DIR + '/controller/MeasurementModule/Evaluation'
    if not os.path.exists(usecase_dir):
        os.makedirs(usecase_dir)

    measure_ram()


def print_rs(length, str):
    print("-" * ((length - len(str)) / 2) + str + "-" * ((length - len(str)) / 2))
