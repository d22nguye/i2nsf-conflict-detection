import matplotlib.pyplot as plt
import glob, os, module
import shutil
import pandas as pd
import numpy as np


def show_all_execution_time():
    DIR = module.get_dir_project()
    file_execution_time = sorted(glob.glob(DIR + "/controller/MeasurementModule/Evaluation/*.txt"))

    for file in file_execution_time:
        name = os.path.basename(file)
        show_execution_time(name)


def show_execution_time(name):
    DIR = module.get_dir_project()

    y = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name, 'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1]
            y.append(float(current_place))

    draw_execution_time(y)


def show_ram_usage(name):
    DIR = module.get_dir_project()

    y = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name, 'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1]
            y.append(float(current_place))
    draw_ram_usage(y)


def save_execution_time(y, mode, rule_number):
    DIR = module.get_dir_project()

    if mode == 1:
        name = "RuBST"
    elif mode == 0:
        name = "NCDABAC"
    elif mode == 4:
        name = "Low-level detection"
    elif mode == 3:
        name = "High-level detection"
    elif mode == 2:
        name = "High-level separation"

    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name + '_' + str(
            rule_number) + '.txt', 'w') as filehandle:
        for list_item in y:
            filehandle.write('%s\n' % list_item)


def save_ram_usage(mode, rule_number):
    DIR = module.get_dir_project()

    if mode == 1:
        name = "RuBST-ram"
    elif mode == 0:
        name = "NCDABAC-ram"
    elif mode == 4:
        name = "Low-level detection-ram"
    elif mode == 3:
        name = "High-level detection-ram"
    elif mode == 2:
        name = "High-level separation-ram"

    shutil.copyfile(DIR + '/controller/HighLevelPolicy/Usecase/ram_temp.txt',
                    DIR + '/controller/MeasurementModule/Evaluation/' + name + '_' + str(
                        rule_number) + '.txt')


def save_exec_time_total(total_exec, mode, rule_number):
    DIR = module.get_dir_project()

    if mode == 1:
        name = "RuBST-total"
    elif mode == 0:
        name = "NCDABAC-total"
    elif mode == 4:
        name = "Low-level detection-total"
    elif mode == 3:
        name = "High-level detection-total"
    elif mode == 2:
        name = "High-level separation-total"

    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name + '.txt', 'a') as filehandle:
        filehandle.write('%s\n' % (str(total_exec) + "," + str(rule_number)))


def save_ram_usage_total(mode, rule_number):
    DIR = module.get_dir_project()

    if mode == 1:
        name = "RuBST-ram-total"
    elif mode == 0:
        name = "NCDABAC-ram-total"
    elif mode == 4:
        name = "Low-level detection-ram-total"
    elif mode == 3:
        name = "High-level detection-ram-total"
    elif mode == 2:
        name = "High-level separation-ram-total"

    shutil.copyfile(DIR + '/controller/HighLevelPolicy/Usecase/ram_temp.txt',
                    DIR + '/controller/MeasurementModule/Evaluation/' + name + '_' + str(
                        rule_number) + '.txt')

    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name + '.txt',
              'a') as filehandleroot:
        with open(DIR + '/controller/HighLevelPolicy/Usecase/max_ram_temp.txt', 'r') as filehandle:
            for line in filehandle:
                filehandleroot.write('%s\n' % (str(line.replace("\n", "")) + "," + str(rule_number)))


def save_infor_generation(i):
    DIR = module.get_dir_project()
    shutil.copyfile(DIR + '/controller/MeasurementModule/Evaluation/save_info_generation.txt',
                    DIR + '/controller/MeasurementModule/Evaluation/save_info_generation_' + str(
                        i) + '_.txt')


def draw_ram_usage(y):
    x = [i for i in range(len(y))]
    y_temp = [i for i in y]
    draw(x, y, "Second (s)", "RAM usage (MB)", "Memory")


def draw_execution_time(y):
    x = [i for i in range(len(y))]
    draw(x, y, "Rule Number", "Execution time (s)", "Performance")


def draw(x, y, x_name, y_name, name):
    plt.plot(x, y)
    # plt.title(name)
    plt.xlabel(x_name, fontsize=16)
    plt.ylabel(y_name, fontsize=16)
    plt.yticks(fontsize=12, )
    plt.xticks(fontsize=12, )
    plt.show()


def draw_with_plot(x, y, x_name, y_name, name):
    plt.plot(x, y, marker='o', c='g')
    plt.xlabel(x_name, fontsize=16)
    plt.ylabel(y_name, fontsize=16)
    plt.yticks(fontsize=12, )
    plt.xticks(fontsize=12, )
    # plt.title(name)
    plt.show()


def draw_bar(x, y, x_name, y_name, name):
    plt.bar(x, y, color='g', label='File Data')
    plt.xlabel(x_name, fontsize=16)
    plt.ylabel(y_name, fontsize=16)
    plt.yticks(fontsize=12, )
    plt.xticks(fontsize=12, )
    # plt.title(name, fontsize=20)
    plt.legend()
    plt.show()


def show_exec_time_total(name, x_name):
    DIR = module.get_dir_project()

    x = []
    y = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name, 'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1].split(",")
            y.append(float(current_place[0]))
            x.append(str(current_place[1]))

    draw_with_plot(x, y, x_name, "Execution time (s)", name + " - Performance")


def show_ram_total(name, x_name):
    DIR = module.get_dir_project()

    x = []
    y = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + name, 'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1].split(",")
            y.append(float(current_place[0]))
            x.append(str(current_place[1]))

    draw_bar(x, y, x_name, "RAM usage (MB)", "Memory")


def draw_ram_performance_overhead(file_performance, file_ram, x_label, y_1_label, y_2_label, is_log_y1_scale,
                                  is_log_y2_scale, start_lim_y1_scale,
                                  end_lim_y1_scale, start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom):
    DIR = module.get_dir_project()
    x = []
    y1 = []
    y1_temp = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_performance,
              'r') as filehandle:
        i = 0
        for line in filehandle:
            current_place = line[:-1].split(",")
            if (i % 2) == 0:
                y1_temp.append(float(current_place[0]))
            else:
                y1.append((y1_temp[-1] - float(current_place[0])) / y1_temp[-1] * 100)
                x.append(str(current_place[1]))
            i += 1

    y2 = []
    y2_temp = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_ram, 'r') as filehandle:
        i = 0
        for line in filehandle:
            current_place = line[:-1].split(",")
            if (i % 2) == 0:
                y2_temp.append(float(current_place[0]))
            else:
                y2.append((y2_temp[-1] - float(current_place[0])) / y2_temp[-1] * 100)
            i += 1
    draw_bar_line(x, y1, y2, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale, start_lim_y1_scale,
                  end_lim_y1_scale,
                  start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom)


def draw_ram_performance_percentage(file_performance, file_ram, x_label, y_1_label, y_2_label, is_log_y1_scale,
                                    is_log_y2_scale, start_lim_y1_scale,
                                    end_lim_y1_scale, start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom):
    DIR = module.get_dir_project()
    x = []
    y1 = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_performance,
              'r') as filehandle:
        i = 0
        for line in filehandle:
            if not i:
                temp = float(line[:-1].split(",")[0])
            else:
                current_place = line[:-1].split(",")
                y1.append((float(current_place[0]) - temp) / temp * 100)
                x.append(str(current_place[1]))
            i += 1
    y2 = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_ram, 'r') as filehandle:
        i = 0
        for line in filehandle:
            if not i:
                temp = float(line[:-1].split(",")[0])
            else:
                current_place = line[:-1].split(",")
                y2.append((float(current_place[0]) - temp) / temp * 100)
            i += 1
    draw_bar_line(x, y1, y2, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale, start_lim_y1_scale,
                  end_lim_y1_scale,
                  start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom)


def draw_ram_performance_log(file_performance, file_ram, x_label, y_1_label, y_2_label, is_log_y1_scale,
                             is_log_y2_scale, start_lim_y1_scale,
                             end_lim_y1_scale, start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom):
    DIR = module.get_dir_project()
    y1 = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_performance,
              'r') as filehandle:
        for line in filehandle:
            y1.append(float(line[:-1]))
    DIR = module.get_dir_project()
    y2_temp = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_ram, 'r') as filehandle:
        for line in filehandle:
            y2_temp.append(float(line[:-1]))

    x = [2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000]
    y2 = [val for val in y2_temp for _ in range(10000 / len(y2_temp) + 1)]
    y2 = y2[:10000]

    y1 = [y1[i - 1] for i in x]
    y2 = [y2[i - 1] for i in x]

    draw_bar_line(x, y1, y2, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale, start_lim_y1_scale,
                  end_lim_y1_scale,
                  start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom)


def draw_ram_performance(file_performance, file_ram, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale,
                         start_lim_y1_scale,
                         end_lim_y1_scale, start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom):
    DIR = module.get_dir_project()
    x = []
    y1 = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_performance,
              'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1].split(",")
            y1.append(float(current_place[0]))
            x.append(str(current_place[1]))

    DIR = module.get_dir_project()
    y2 = []
    with open(DIR + '/controller/MeasurementModule/Evaluation/' + file_ram, 'r') as filehandle:
        for line in filehandle:
            current_place = line[:-1].split(",")
            y2.append(float(current_place[0]))
    draw_bar_line(x, y1, y2, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale, start_lim_y1_scale,
                  end_lim_y1_scale,
                  start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom)

def draw_bar_line(x, y1, y2, x_label, y_1_label, y_2_label, is_log_y1_scale, is_log_y2_scale, start_lim_y1_scale,
                  end_lim_y1_scale,
                  start_lim_y2_scale, end_lim_y2_scale, left, right, top, bottom):
    plt.rcParams["figure.figsize"] = [14, 14]
    # plt.rcParams["figure.autolayout"] = True
    df = pd.DataFrame(dict(x=x, y1=y1, y2=y2))
    x_pos = np.arange(len(df.index))
    # plt.rcParams['figure.figsize'] = (18, 8)
    plt.rcParams["font.size"] = 22
    fig, ax = plt.subplots()
    # plt.subplots_adjust(left=0.09, right=0.91, top=0.98, bottom=0.1)
    plt.subplots_adjust(left=left, right=right, top=top, bottom=bottom)
    plt.xticks(x_pos, df['x'])

    if is_log_y1_scale:
        ax.set_yscale('log')
    ax.plot(x_pos, df['y1'], color='C3', marker='D', ms=7)
    if start_lim_y1_scale > -1:
        ax.set_ylim([start_lim_y1_scale, end_lim_y1_scale])
    ax2 = ax.twinx()
    if is_log_y2_scale:
        ax2.set_yscale('log')
    if start_lim_y2_scale > -1:
        ax2.set_ylim([start_lim_y2_scale, end_lim_y2_scale])
    ax.set_zorder(ax2.get_zorder() + 1)
    ax.set_frame_on(False)
    ax2.bar(x_pos, df['y2'], color='C0', width=0.5)
    ax.tick_params(axis='y', colors='C3')
    ax2.tick_params(axis='y', colors='C0')
    ax.xaxis.label.set_color('black')
    ax2.xaxis.label.set_color('black')
    ax.grid(False)
    ax2.grid(False)
    # plt.title('Transactions by Merchant Category')
    ax.set_xlabel(x_label, fontsize=30)
    ax.set_ylabel(y_1_label, fontsize=30)
    ax2.set_ylabel(y_2_label, rotation=270, labelpad=33, fontsize=30)
    plt.show()


if __name__ == "__main__":
    # show_all_execution_time()
    # plt.subplots_adjust(left=0.14, right=0.99, top=0.98, bottom=0.12)
    # show_execution_time("High-level separation_10000.txt")
    # show_ram_usage("High-level separation-ram_10000.txt")

    draw_ram_performance_log("High-level separation_10000_logscale.txt",
                             "High-level separation-ram_10000_logscale.txt", "Number of rules", "Execution Time (s)",
                             "Memory usage (MB)", True, False, -1, -1, 65, 285, 0.1, 0.91, 0.98, 0.09)

    draw_ram_performance("High-level separation-total_number_conflict.txt",
                         "High-level separation-ram-total_number_conflict.txt", "Number of conflicts",
                         "Execution Time (s)",
                         "Memory usage (MB)", False, False, 0, 200, 200, 400, 0.09, 0.91, 0.98, 0.09)

    draw_ram_performance("High-level separation-total_percentage_explicit.txt",
                         "High-level separation-ram-total_percentage_explicit.txt", "Percentage of explicit conflicts",
                         "Execution Time (s)",
                         "Memory usage (MB)", False, False, 0, 200, 200, 400, 0.09, 0.91, 0.98, 0.09)

    draw_ram_performance("High-level separation-total-unchange-rule.txt",
                         "High-level separation-ram-total-unchangerule.txt",
                         "|Number of \"pass\" rules - Number of \"drop\" rules|", "Execution Time (s)",
                         "Memory usage (MB)", False, False, -1, -1, 0, 500, 0.09, 0.91, 0.98, 0.09)

    draw_ram_performance_percentage("High-level separation-total_change_separation.txt",
                                    "High-level separation-ram-total_change_separation.txt", "Percentage of SC",
                                    "Percentage of Execution Time overhead (%)",
                                    "Percentage of Memory usage overhead (%)", False, False, 0, 20, -1, -1, 0.097, 0.89,
                                    0.98, 0.09)

    draw_ram_performance_overhead("High-level separation-total_conflict_with_SC.txt",
                                  "High-level separation-ram-total_conflict_with_SC.txt", "Number of conflicts",
                                  "Percentage of Execution Time overhead (%)",
                                  "Percentage of Memory usage overhead (%)", False, False, 0, 30, 0, 4, 0.08, 0.92,
                                  0.97, 0.09)

    # draw_ram_performance("High-level separation-total_increaserule_noconflict.txt",
    #                      "High-level separation-ram-total_increaserule_noconflict.txt", "Number of rules",
    #                      "Execution Time (s)",
    #                      "Memory usage (MB)", True, False, -1, -1, -1, -1, 0.096, 0.88, 0.98, 0.1)

    # draw_ram_performance("High-level separation-total-same-rule-10-time.txt",
    #                      "High-level separation-ram-total-same-rule-10-time.txt", "Run times", "Execution Time (s)",
    #                      "Memory usage (MB)", 0, 200, 280, 300)

    # draw_ram_performance("High-level separation-total_increaserule.txt",
    #                      "High-level separation-ram-total_increaserule.txt", "Number of rules", "Execution Time (s)",
    #                      "Memory usage (MB)")

    # show_exec_time_total("High-level separation-total_percentage_explicit.txt", "Number of rules")
    # show_ram_total("High-level separation-ram-total_increaserule.txt", "Number of rules")

    # plt.subplots_adjust(left=0.05, right=0.99, top=0.98, bottom=0.07)
    # show_exec_time_total("High-level separation-total_pass_drop.txt", "% \"pass\" rules")
    # plt.subplots_adjust(left=0.05, right=0.99, top=0.98, bottom=0.07)
    # show_ram_total("High-level separation-ram-total_pass_drop.txt", "% \"pass\" rules")
    #
    # plt.subplots_adjust(left=0.05, right=0.99, top=0.98, bottom=0.07)
    # show_exec_time_total("High-level separation-total-unchange-rule.txt", "Run times")
    # plt.subplots_adjust(left=0.05, right=0.99, top=0.98, bottom=0.07)
    # show_ram_total("High-level separation-ram-total-unchangerule.txt", "Run times")
    #
    # plt.subplots_adjust(left=0.11, right=0.99, top=0.98, bottom=0.12)
    # show_exec_time_total("High-level separation-total-1-5.txt", "Number of rules")
    # plt.subplots_adjust(left=0.12, right=0.99, top=0.98, bottom=0.12)
    # show_exec_time_total("High-level separation-total-6-10.txt","Separation Constraint Number")
    # plt.subplots_adjust(left=0.12, right=0.99, top=0.98, bottom=0.12)
    # show_ram_total("High-level separation-ram-total-1-5.txt","Number of rules")
    # plt.subplots_adjust(left=0.12, right=0.99, top=0.98, bottom=0.12)
    # show_ram_total("High-level separation-ram-total-6-10.txt", "Separation Constraint Number")
