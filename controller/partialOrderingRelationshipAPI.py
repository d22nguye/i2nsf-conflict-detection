import API.DFAAPI as DFA


class PartialOrderingRelationship:
    def __init__(self, consumer_dfa, consumer_extractedinfo):
        self.consumer_dfa = consumer_dfa                        # extractor instance
        self.consumer_extractedinfo = consumer_extractedinfo    # attribute identifiers
        self.list_existing_priority_rules = []                  # set of partial ordering relationships

    def deploy_partial_ordering_relationship(self, filename):
        # ******************************************************************************************
        # + filename: Name of file contains partial ordering relationship
        # Deploy partial ordering relationship declared in file
        # ******************************************************************************************

        # list of attribute value
        consumer_extractedlist = DFA.extracting_data(filename, self.consumer_dfa, self.consumer_extractedinfo)

        if consumer_extractedlist[82]:

            # if relationship is invalid
            if not self.is_partial_ordering_validated(consumer_extractedlist[82]):
                # may add notification to user in case of invalid relationship
                print "Fail to deploy partial ordering relationship ", consumer_extractedlist[82][0], " and ", \
                    consumer_extractedlist[82][1]
                return consumer_extractedlist[82]

            # accept partial ordering relationship if it is valid
            self.list_existing_priority_rules.append(consumer_extractedlist[82])

    def is_partial_ordering_validated(self, por):
        # ******************************************************************************************
        # + por: partial odering relationship
        # Return True if POR is valid. Return False otherwise
        # ******************************************************************************************

        rule_name_1 = por[0] # rule_name_1: the first priority rule R1
        rule_name_2 = por[1] # rule_name_2: the second priority rule R2

        # check all existing POR
        for element in self.list_existing_priority_rules:

            # if both R1, R2 are included in a POR of the set
            if (rule_name_1 in element) and (rule_name_2 in element):

                # if R1 is the second priority rule in this existing POR
                if rule_name_1 == element[1]:
                    return False
        return True
