import API.DFAAPI as DFA
import MySQLdb


def split_ip(ip):
    return tuple(int(part) for part in ip.split('.'))


def is_less_ip(ip1, ip2):
    if (not ip1) or (not ip2):
        return False
    split_ip_1 = split_ip(ip1)
    split_ip_2 = split_ip(ip2)
    for i in range(4):
        if split_ip_1[i] != split_ip_2[i]:
            return split_ip_1[i] < split_ip_2[i]
    return False


class SeparationConstraint:
    def __init__(self, consumer_dfa, consumer_extractedinfo):
        self.consumer_dfa = consumer_dfa  # extractor instance
        self.consumer_extractedinfo = consumer_extractedinfo  # attribute identifiers
        self.map_separation_not_share_value = {}  # map of separation constraints

    def deploy_separation_constraint(self, filename):
        # ******************************************************************************************
        # + filename: Name of file contains separation constraint
        # Deploy separation constraint declared in file
        # ******************************************************************************************

        # list of attribute value
        consumer_extractedlist = DFA.extracting_data(filename, self.consumer_dfa, self.consumer_extractedinfo)

        if consumer_extractedlist[80]:
            # get attribute identifier declared in separation constraint
            att_id = consumer_extractedlist[80][0]

            # a considered usecase
            if att_id == 'src-target':
                # position of src-target in consumer-facing data model
                att_id = 11

                # if separation constraint of src-target (IP address) is validated
                if not self.is_separation_constraint_ip_validated(consumer_extractedlist[81]):
                    # may add notification to user in case of invalid separation constraint
                    print "Fail to deploy separation constraint ", consumer_extractedlist[81][0], " and ", \
                        consumer_extractedlist[81][1]
                    return consumer_extractedlist[81]

            # exit if API does not handle attribute
            else:
                return consumer_extractedlist[81]

            # if it exists other SC on the considered attribute identifier
            if att_id in self.map_separation_not_share_value:
                value_1 = consumer_extractedlist[81][0] # first value of SC
                value_2 = consumer_extractedlist[81][1] # second value of SC

                # add key value_1 and value_2 to map which contain the other one respectively
                if value_1 in self.map_separation_not_share_value[att_id]:
                    self.map_separation_not_share_value[att_id][value_1].append(value_2)
                else:
                    self.map_separation_not_share_value[att_id][value_1] = [value_2]

                if value_2 in self.map_separation_not_share_value[att_id]:
                    self.map_separation_not_share_value[att_id][value_2].append(value_1)
                else:
                    self.map_separation_not_share_value[att_id][value_2] = [value_1]
            else:
                self.map_separation_not_share_value[att_id] = {
                    consumer_extractedlist[81][0]: [consumer_extractedlist[81][1]],
                    consumer_extractedlist[81][1]: [consumer_extractedlist[81][0]]}

    def separation_constraint_violation_validate(self, rule_1, rule_2):
        # ******************************************************************************************
        # + rule_1: Rule R1
        # + rule_2: Rule R2
        # Return True if R1 and R2 do not contain the two value specified by any SC. Return False otherwise
        # ******************************************************************************************

        for key in self.map_separation_not_share_value:
            separation_not_share_rules = self.map_separation_not_share_value[key]
            if (rule_1[key] != []) and (rule_2[key] != []) and (rule_1[key][0] != rule_2[key][0]):
                if rule_1[key][0] in separation_not_share_rules:
                    list_separation = separation_not_share_rules[rule_1[key][0]]
                    if rule_2[key][0] in list_separation:
                        return False
        return True

    def is_separation_constraint_ip_validated(self, sc):
        # ******************************************************************************************
        # + sc: separation constraint
        # Return True if SC is valid. Return False otherwise
        # ******************************************************************************************

        src_target_1 = sc[0]
        src_target_2 = sc[1]

        start_ip_1, end_ip_1, start_ip_2, end_ip_2 = '', '', '', ''

        nsfdb = MySQLdb.connect(host="localhost", user="root", passwd="secu", db="nsfdb")
        nsfcur = nsfdb.cursor()
        nsfcur.execute("SELECT id,data FROM endpointtable WHERE ename='" + src_target_1 + "'")
        rows = nsfcur.fetchall()
        for ptr in rows:
            if ptr[0] == 43:
                start_ip_1 = ptr[1]
            elif ptr[0] == 44:
                end_ip_1 = ptr[1]

        nsfcur.execute("SELECT id,data FROM endpointtable WHERE ename='" + src_target_2 + "'")
        rows = nsfcur.fetchall()
        for ptr in rows:
            if ptr[0] == 43:
                start_ip_2 = ptr[1]
            elif ptr[0] == 44:
                end_ip_2 = ptr[1]
        nsfcur.close()
        nsfdb.close()

        if is_less_ip(end_ip_2, start_ip_1) | is_less_ip(end_ip_1, start_ip_2):
            return True
        return False
