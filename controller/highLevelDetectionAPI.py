class High_level_detection:

    def __init__(self, list_attribute_extracted):
        self.list_attribute_extracted = list_attribute_extracted  # attribute identifiers

    def is_different_action_high_level(self, rule_1, rule_2):
        # ******************************************************************************************
        # + rule_1: Rule R1
        # + rule_2: Rule R2
        # Return True if R1 and R2 share the same action. Return False otherwise
        # ******************************************************************************************

        return ((rule_1[20][0] == "drop") & (rule_2[20][0] == "pass")) | (
                (rule_1[20][0] == "pass") & (rule_2[20][0] == "drop"))

    def high_level_exist_nonoverlapped_ap(self, rule_1, rule_2):
        # ******************************************************************************************
        # + rule_1: Rule R1
        # + rule_2: Rule R2
        # Return True if R1 and R2 have non-overlapped attribute expression. Return False otherwise
        # ******************************************************************************************

        # process all attributes
        for i in range(len(self.list_attribute_extracted)):
            att = self.list_attribute_extracted[i]
            if ("action" not in att) & ("policy-name" not in att) & (
                    "rule-name" not in att) & ("src-target" not in att) & ("src-target" not in att) & (
                    "dest-target" not in att):
                ap_1 = rule_1[i]
                ap_2 = rule_2[i]

                # only check existing value since absent attribute is deemed overlapping
                if (ap_1 != []) & (ap_2 != []):

                    # if attribute value is [begin, end]
                    if "begin" in att:
                        if "end" in self.list_attribute_extracted[i + 1]:
                            start_ap_1 = ap_1[0]
                            start_ap_2 = ap_2[0]
                            end_ap_1 = rule_1[i + 1][0]
                            end_ap_2 = rule_2[i + 1][0]

                            if (start_ap_1 > end_ap_2) | (end_ap_1 < start_ap_2):
                                return True
                    # not consider again "end" value
                    elif "end" in att:
                        continue
                    # if attribute value is a set
                    elif not set(ap_1).intersection(ap_2):
                        return True
                    # if attribute value is a string
                    elif ((len(ap_1) == 1) & (len(ap_2) == 1)) and (ap_1 != ap_2):
                        return True
        return False

    def detect(self, rule_1, rule_2):
        # ******************************************************************************************
        # + rule_1: Rule R1
        # + rule_2: Rule R2
        # Return ture if R1 and R2 are conflicting rules. Return False otherwise
        # ******************************************************************************************

        if self.is_different_action_high_level(rule_1, rule_2):
            if not self.high_level_exist_nonoverlapped_ap(rule_1, rule_2):
                return True
        return False
