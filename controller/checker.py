import separationConstraintAPI, highLevelDetectionAPI, partialOrderingRelationshipAPI
import time


class Checker:
    def __init__(self, list_rules, consumer_dfa, consumer_extractedinfo, is_measure):
        self.list_rules = list_rules  # list of rules

        # SC API instance
        self.SC_API = separationConstraintAPI.SeparationConstraint(consumer_dfa, consumer_extractedinfo)

        # HL API instance
        self.HLDetection = highLevelDetectionAPI.High_level_detection(consumer_extractedinfo)

        # PO API instance
        self.PO_API = partialOrderingRelationshipAPI.PartialOrderingRelationship(consumer_dfa, consumer_extractedinfo)

        self.list_conflicting_rules = []  # list of conflicting rules of a new rule deployed
        self.total_conflicting_rules = []  # total list of conflicting rules

        self.is_measure = is_measure  # for evaluation
        self.list_execution_time = []  # for evaluation

    def insert_new_rule(self, dataconverter):
        # ******************************************************************************************
        # + dataconverter: data between extractor and converter
        # Store new data extracted of a new deployed rule
        # ******************************************************************************************

        self.list_rules.append(dataconverter.extractedlist[:])

    def checkConflict(self, extracted_data):
        # ******************************************************************************************
        # + extracted_data: new extracted data from a new deployed rule
        # Calculate a list of conflicting rules
        # ******************************************************************************************

        # for evaluation
        if self.is_measure:
            st = time.time()

        self.list_conflicting_rules = []  # list of conflicting rules

        # process all deployed rules
        for i in range(len(self.list_rules)):
            rule_1_extracted = self.list_rules[i]  # old deployed rule R1
            rule_2_extracted = extracted_data  # new deployed rule R2

            # if violate separation constraint
            if self.SC_API.separation_constraint_violation_validate(rule_1_extracted, rule_2_extracted):

                # if R1 and R2 are conflicting rules in attributes and actions
                if self.HLDetection.detect(rule_1_extracted, rule_2_extracted):
                    self.list_conflicting_rules.append([rule_1_extracted[1], rule_2_extracted[1]])
                    self.total_conflicting_rules.append([rule_1_extracted[1], rule_2_extracted[1]])

        # for evaluation
        if self.is_measure:
            et = time.time()
            elapsed_time = et - st
            self.list_execution_time.append(elapsed_time)
