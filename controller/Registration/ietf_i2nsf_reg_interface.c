/*********************************************************************
 * ConfD Subscriber intro example
 * Implements a DHCP server adapter
 *
 * (C) 2005-2007 Tail-f Systems
 * Permission to use this code as a starting point hereby granted
 *
 * See the README file for more information
 ********************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>

#include <confd_lib.h>
#include <confd_cdb.h>

/* include generated file */
#include "ietf-i2nsf-capability.h"
#include "ietf-i2nsf-reg-interface.h"
#include "ietf-i2nsf-feedback-policy.h"
#include "ietf-i2nsf-policy-rule-for-nsf.h"
/********************************************************************/

int rule_id = 40000;
int confd_start = 1;
char exist_nsf[500] = "";


static int read_conf(struct sockaddr_in *addr)
{
    FILE *fp;
    int nsf_num, rule_num;
    int i, j, k;
    int rsock;
    char nsf_name[300];
    confd_value_t *time_capa;
    int time_capa_num;
    confd_value_t *ipv4_capa;
    int ipv4_capa_num;
    confd_value_t *tcp_capa;
    int tcp_capa_num;
    confd_value_t *url_filtering_capability;
    int url_filtering_capability_num;
    confd_value_t *voice_capa;
    int voice_capa_num;
    confd_value_t *antiddos_capa;
    int antiddos_capa_num;
    confd_value_t *ingress_action_capa;
    int ingress_action_capa_num;
     confd_value_t *egress_action_capa;
    int egress_action_capa_num;
    char *temp_exist_nsf_name;
    char temp_exist_nsf[300];
    int exist = 0;
    char nsf_capa[1000];
    u_int16_t processing_average;
    u_int16_t processing_peak;
    u_int16_t outbound_average;
    u_int16_t outbound_peak;
    u_int16_t inbound_average;
    u_int16_t inbound_peak;
    char str_processing_average[100];
    char str_processing_peak[100];
    char str_outbound_average[100];
    char str_outbound_peak[100];
    char str_inbound_average[100];
    char str_inbound_peak[100];

    if ((rsock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
        confd_fatal("Failed to open socket\n");

    if (cdb_connect(rsock, CDB_READ_SOCKET, (struct sockaddr*)addr,
                      sizeof (struct sockaddr_in)) < 0)
        return CONFD_ERR;
    if (cdb_start_session(rsock, CDB_RUNNING) != CONFD_OK)
        return CONFD_ERR;
    cdb_set_namespace(rsock, nsfreg__ns);

    cdb_cd(rsock, "nsf-registrations");
    nsf_num = cdb_num_instances(rsock, "nsf-information");

    printf("NSF Num: %d\n", nsf_num);

    for (j = 0; j < nsf_num; j++) {
        printf("Current Num: %d\n", j);
        //cdb_get_str(rsock, &nsf_name[0], sizeof(nsf_name), "nsf-name");
        //printf("nsf-name: %s\n\n",nsf_name);

	for (k = 0; k < 1000; k++) {
		nsf_capa[k] = " ";
	} 	

        cdb_get_str(rsock, &nsf_name[0], sizeof(nsf_name), "nsf-information[%d]/nsf-name",j);
        printf("nsf-name: %s\n\n",nsf_name);
	strncpy(temp_exist_nsf,exist_nsf,sizeof(temp_exist_nsf));
	temp_exist_nsf_name = strtok(temp_exist_nsf, ",");
	while(temp_exist_nsf_name != NULL)
	{
		printf("\nTest: %s\n", temp_exist_nsf_name);
		if (strncmp(temp_exist_nsf_name,nsf_name,sizeof(nsf_name)) == 0) {
			exist = 1;
		}
		temp_exist_nsf_name = strtok(NULL,",");
	}
	if (exist == 1) {
		exist = 0;
		continue;
	}

        cdb_pushd(rsock, "nsf-information[%d]", j);
	cdb_get_str(rsock, &nsf_name[0], sizeof(nsf_name), "nsf-name");
	strncat(exist_nsf,nsf_name,sizeof(nsf_name));
	strncat(exist_nsf,", ",1);
	printf("Exist NSF: %s\n\n", exist_nsf);
	strncpy(nsf_capa,"\nnsf-name: ",sizeof("\nnsf-name: "));
	strncat(nsf_capa,nsf_name,sizeof(nsf_name));
//	strncat(nsf_capa,",",1);


        cdb_cd(rsock, "nsf-capability-info/security-capability/condition-capabilities/context-capabilities");
	if (cdb_get_list(rsock, &time_capa, &time_capa_num, "time-capabilities") == CONFD_ERR) {
		time_capa_num = 0;
	}
	else {
		strncat(nsf_capa,"\ntime-capabilities: ",sizeof("\ntime-capabilities: "));	
		for (i = 0; i < time_capa_num; i++) {
	                switch(CONFD_GET_IDENTITYREF(&time_capa[i]).id) {
	                        case nsfcap_absolute_time:
	                                printf("Time Capa: %s\n", "absolute-time");
					strncat(nsf_capa,"absolute-time",sizeof("absolute-time"));
	                                break;
	                        case nsfcap_periodic_time:
	                                printf("Time Capa: %s\n", "periodic-time");
					strncat(nsf_capa,"periodic-time",sizeof("periodic-time"));
                        	        break;
                	}
			if (i < (time_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}
		}
	}


        cdb_cd(rsock, "../generic-nsf-capabilities");
	if (cdb_get_list(rsock, &ipv4_capa, &ipv4_capa_num, "ipv4-capability") == CONFD_ERR) {
		ipv4_capa_num = 0;
	} else {
		strncat(nsf_capa,"\nipv4-capability: ",sizeof("\nipv4-capability: "));	
		for (i = 0; i < ipv4_capa_num; i++) {
                	switch(CONFD_GET_IDENTITYREF(&ipv4_capa[i]).id) {
        	                case nsfcap_next_header:
	                                printf("IPv4 Capability: %s\n", "next-header");
					strncat(nsf_capa,"next-header",sizeof("next-header"));
                	                break;
        	                case nsfcap_source_address:
	                                printf("IPv4 Capability: %s\n", "source-address");
					strncat(nsf_capa,"source-address",sizeof("source-address"));
                	                break;
        	                case nsfcap_destination_address:
	                                printf("IPv4 Capability: %s\n", "destination-address");
					strncat(nsf_capa,"destination-address",sizeof("destination-address"));
                        	        break;
        	        }
			if (i < (ipv4_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}

	}

	if (cdb_get_list(rsock, &tcp_capa, &tcp_capa_num, "tcp-capability") == CONFD_ERR) {
		tcp_capa_num = 0;
	} else {
		strncat(nsf_capa,"\ntcp-capability: ",sizeof("\ntcp-capability: "));	
		for (i = 0; i < tcp_capa_num; i++) {
                	switch(CONFD_GET_IDENTITYREF(&tcp_capa[i]).id) {
        	                case nsfcap_source_port_number:
	                                printf("TCP Capa: %s\n", "source-port-number");
					strncat(nsf_capa,"source-port-number",sizeof("source-port-number"));
                	                break;
        	                case nsfcap_destination_port_number:
	                                printf("TCP Capa: %s\n", "destination-port-number");
					strncat(nsf_capa,"destination-port-number",sizeof("destination-port-number"));
                	                break;
        	        }
			if (i < (tcp_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}
	}

        cdb_cd(rsock, "../advanced-nsf-capabilities");
	if (cdb_get_list(rsock, &url_filtering_capability, &url_filtering_capability_num, "url-filtering-capability") == CONFD_ERR) {
		url_filtering_capability_num = 0;
	} else {
		strncat(nsf_capa,"\nurl-filtering-capability: ",sizeof("\nurl-filtering-capability: "));	
		for (i = 0; i < url_filtering_capability_num; i++) {
                	switch(CONFD_GET_IDENTITYREF(&url_filtering_capability[i]).id) {
        	                case nsfcap_user_defined:
	                                printf("HTTP Capa: %s\n", "user-defined");
					strncat(nsf_capa,"user-defined",sizeof("user-defined"));
                	                break;
        	        }
			if (i < (url_filtering_capability_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}
	}

	if (cdb_get_list(rsock, &voice_capa, &voice_capa_num, "voip-volte-capability") == CONFD_ERR) {
		voice_capa_num = 0;
	} else {
		strncat(nsf_capa,"\nvoip-volte-capability: ",sizeof("\nvoip-volte-capability: "));	
		for (i = 0; i < voice_capa_num; i++) {
        	        switch(CONFD_GET_IDENTITYREF(&voice_capa[i]).id) {
                	        case nsfcap_call_id:
                        	        printf("VoIP/VoLTE Capa: %s\n", "call-id");
					strncat(nsf_capa,"voice-call-id",sizeof("call-id"));
        	                        break;
                	}
			if (i < (voice_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}
		}
	}

	if (cdb_get_list(rsock, &antiddos_capa, &antiddos_capa_num, "anti-ddos-capability") == CONFD_ERR) {
		antiddos_capa_num = 0;
	} else {
		strncat(nsf_capa,"\nanti-ddos-capability: ",sizeof("\nanti-ddos-capability: "));	
		for (i = 0; i < antiddos_capa_num; i++) {
        	        switch(CONFD_GET_IDENTITYREF(&antiddos_capa[i]).id) {
                	        case nsfcap_packet_rate:
                        	        printf("Anti DDoS Capa: %s\n", "packet-rate");
					strncat(nsf_capa,"packet-rate",sizeof("packet-rate"));
        	                        break;
                	        case nsfcap_byte_rate:
                        	        printf("Anti DDoS Capa: %s\n", "byte-rate");
					strncat(nsf_capa,"byte-rate",sizeof("byte-rate"));
        	                        break;

                	}
			if (i < (antiddos_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}
	}



        cdb_cd(rsock, "../../action-capabilities");
	if(cdb_get_list(rsock, &ingress_action_capa, &ingress_action_capa_num, "ingress-action-capability") == CONFD_ERR) {
		ingress_action_capa_num = 0;
	} else {
		strncat(nsf_capa,"\ningress-action-capability: ",sizeof("\ningress-action-capability: "));	

		for (i = 0; i < ingress_action_capa_num; i++) {
        	        switch(CONFD_GET_IDENTITYREF(&ingress_action_capa[i]).id) {
                	        case nsfcap_pass:
                        	        printf("Ingress Action Capa: %s\n", "pass");
					strncat(nsf_capa,"pass",sizeof("pass"));
        	                        break;
                	        case nsfcap_drop:
                        	        printf("Ingress Action Capa: %s\n", "drop");
					strncat(nsf_capa,"drop",sizeof("drop"));
        	                        break;
                	        case nsfcap_reject:
                        	        printf("Ingress Action Capa: %s\n", "reject");
					strncat(nsf_capa,"reject",sizeof("reject"));
        	                        break;
                	}
			if (i < (ingress_action_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}

	}

	if(cdb_get_list(rsock, &egress_action_capa, &egress_action_capa_num, "egress-action-capability") == CONFD_ERR) {
		egress_action_capa_num = 0;
	} else {
		strncat(nsf_capa,"\negress-action-capability: ",sizeof("\negress-action-capability: "));	

		for (i = 0; i < egress_action_capa_num; i++) {
        	        switch(CONFD_GET_IDENTITYREF(&egress_action_capa[i]).id) {
                	        case nsfcap_pass:
                        	        printf("Egress Action Capa: %s\n", "pass");
					strncat(nsf_capa,"pass",sizeof("pass"));
        	                        break;
                	        case nsfcap_drop:
                        	        printf("Egress Action Capa: %s\n", "drop");
					strncat(nsf_capa,"drop",sizeof("drop"));
        	                        break;
                	        case nsfcap_reject:
                        	        printf("Egress Action Capa: %s\n", "reject");
					strncat(nsf_capa,"reject",sizeof("reject"));
        	                        break;
                	}
			if (i < (egress_action_capa_num-1)) {
				strncat(nsf_capa,",",1);
			}

		}
	}

        cdb_cd(rsock, "../../performance-capability/");

	strncat(nsf_capa,"\nprocessing: ",sizeof("\nprocessing: "));	
	cdb_get_u_int16(rsock, &processing_average, "processing/processing-average");
	printf("Processing Average: %d\n", processing_average);
	sprintf(str_processing_average,"%d", processing_average);
	strncat(nsf_capa,str_processing_average,sizeof(str_processing_average));
	strncat(nsf_capa,",",1);

	cdb_get_u_int16(rsock, &processing_peak, "processing/processing-peak");
	printf("Processing Peak: %d\n", processing_peak);
	sprintf(str_processing_peak,"%d", processing_peak);
	strncat(nsf_capa,str_processing_peak,sizeof(str_processing_peak));


	strncat(nsf_capa,"\nBandwidth Outbound: ",sizeof("\nBandwidth Outbound: "));	
	cdb_get_u_int16(rsock, &outbound_average, "bandwidth/outbound/outbound-average");
	printf("Outbound Average: %d\n", outbound_average);
	sprintf(str_outbound_average,"%d", outbound_average);
	strncat(nsf_capa,str_outbound_average,sizeof(str_outbound_average));
	strncat(nsf_capa,",",1);

	cdb_get_u_int16(rsock, &outbound_peak, "bandwidth/outbound/outbound-peak");
	printf("Outbound Peak: %d\n", outbound_peak);
	sprintf(str_outbound_peak,"%d", outbound_peak);
	strncat(nsf_capa,str_outbound_peak,sizeof(str_outbound_peak));


	strncat(nsf_capa,"\nBandwidth Inbound: ",sizeof("\nBandwidth Inbound: "));	
	cdb_get_u_int16(rsock, &inbound_average, "bandwidth/inbound/inbound-average");
	printf("Inbound Average: %d\n", inbound_average);
	sprintf(str_inbound_average,"%d", inbound_average);
	strncat(nsf_capa,str_inbound_average,sizeof(str_inbound_average));
	strncat(nsf_capa,",",1);

	cdb_get_u_int16(rsock, &inbound_peak, "bandwidth/inbound/inbound-peak");
	printf("Inbound Peak: %d\n", inbound_peak);
	sprintf(str_inbound_peak,"%d", inbound_peak);
	strncat(nsf_capa,str_inbound_peak,sizeof(str_inbound_peak));

	printf("\n\nNSF Capa: %s\n\n", nsf_capa);

        int client_fd,len;
        struct sockaddr_in client_addr;

        client_fd = socket(PF_INET, SOCK_STREAM, 0);

        client_addr.sin_addr.s_addr = inet_addr("10.0.0.17"); /* EDIT THE IP ADDRESS WITH YOUR SECURITY CONTROLLER */
        client_addr.sin_family = AF_INET;
        client_addr.sin_port = htons(55552);

        if(connect(client_fd,(struct sockaddr *)&client_addr, sizeof(client_addr)) == -1)
        {
                printf("Can't connect\n");
                close(client_fd);
                return -1;
        }

        send(client_fd, (char *)nsf_capa, strlen(nsf_capa)+1, 0);

        close(client_fd);
        printf("\nSend: %s\n", nsf_capa);
        printf("\n\n\n", nsf_capa);


        //do_rule(rsock, fp);
        cdb_popd(rsock);
    }

        /*if (confd_start == 1) {
                sleep(10);
                confd_start = 0;
        }
        system("sudo /usr/bin/suricatasc -c reload-rules /var/run/suricata/suricata-command.socket");
*/
    printf("Success\n");

    return cdb_close(rsock);
}

static void do_rule(int rsock, FILE *fp, char *policy_name)
{
//   struct in_addr *ip_list;
//   struct confd_datetime date_time;
  char date_time[BUFSIZ];

  char temp_rule[30000];
  char temp_itoa[300];
  int i,j;
  char rule_name[BUFSIZ];
  char common_ip_addr[10000];
  char str_src_start_ip_addr[50];
  char str_src_end_ip_addr[50];
  char *start_ptr;
  char str_start_ptr[100];
  char *end_ptr;
  char str_end_ptr[100];
  char temp_str[10];
  int int_start_ptr;
  int int_end_ptr;
  int range_src_ip_num;
  struct confd_identityref ingress_action;
  struct in_addr src_start_ip_addr;
  struct in_addr src_end_ip_addr;
  int advanced_action_num;
  char nsf_name[50];
  struct in_addr nsf_ip;
  char in_action[100];

  for (i=0; i<30000; i++) {
    temp_rule[i] = NULL;
  }

  cdb_get_str(rsock, &rule_name[0], BUFSIZ, "name");
  printf("Rule Name: %s\n",rule_name);

  //cdb_cd(rsock, "../../condition-clause-container/packet-security-ipv4-condition/pkt-sec-ipv4-src");
  cdb_cd(rsock, "condition/ipv4");
  //cdb_cd(rsock, "ipv4");
  range_src_ip_num = cdb_num_instances(rsock, "source-ipv4-range");
  for (i = 0; i < range_src_ip_num; i++) {
    cdb_get_ipv4(rsock, &src_start_ip_addr,"source-ipv4-range[%d]/start", i);
    strncpy(str_src_start_ip_addr,inet_ntoa(src_start_ip_addr),sizeof(str_src_start_ip_addr));
    printf("Src Start IP: %s\n", str_src_start_ip_addr);
    cdb_get_ipv4(rsock, &src_end_ip_addr,"source-ipv4-range[%d]/end", i);
    strncpy(str_src_end_ip_addr,inet_ntoa(src_end_ip_addr),sizeof(str_src_end_ip_addr));
    printf("Src End IP: %s\n", str_src_end_ip_addr);
  }
  confd_value_t v;
  cdb_cd(rsock, "../../action/packet-action");
  cdb_get(rsock,&v, "ingress-action");
//  printf("Packet-action:%d\n",v[0].val);
  ingress_action = CONFD_GET_IDENTITYREF(&v);
  printf("pointer address of ingress_action: %p\n", ingress_action);
  printf("Ingress action: %d\n", ingress_action.id);
  switch(ingress_action.id) {
    case nsfintf_pass:
      printf("Ingress Action: %s\n", "pass");
      strncat(in_action,"pass",sizeof("pass"));
      break;
    case nsfintf_drop:
      printf("Ingress Action: %s\n", "drop");
      strncat(in_action,"drop",sizeof("drop"));
      break;
    case nsfintf_reject:
      printf("Ingress Action: %s\n", "reject");
      strncat(in_action,"reject",sizeof("reject"));
      break;
  }


  cdb_cd(rsock, "../../../");
  cdb_get_ipv4(rsock, &nsf_ip, "nsf-name");
  strncpy(nsf_name, inet_ntoa(nsf_ip),sizeof(nsf_name));
  printf("NSF Name:%s\n", nsf_name);
  /*for (i = 0; i < advanced_action_num; i++) {
    printf("Packet action: %d\n", CONFD_GET_IDENTITYREF(&advanced_action[i]).ns);
    printf("Packet action: %d\n", CONFD_GET_IDENTITYREF(&advanced_action[i]).id);
    switch(CONFD_GET_IDENTITYREF(&advanced_action[i]).id) {
      case nsfintf_drop:
        printf("Packet Action: %s\n", "drop");
        break;
    }
  }*/
  FILE *file;
  file  = fopen ("/home/ubuntu/LowLevelPolicy/feedback.xml", "w");
  fprintf(file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(file,"<hello xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n");
  fprintf(file,"<capabilities>\n");
  fprintf(file,"<capability>urn:ietf:params:netconf:base:1.0</capability>\n");
  fprintf(file,"</capabilities>\n");
  fprintf(file,"</hello>\n");
  fprintf(file,"]]>]]>\n");
  fprintf(file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(file,"<rpc message-id=\"1\" xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n");
  fprintf(file,"<edit-config>\n");
  fprintf(file," <target>\n");
  fprintf(file,"<running />\n");
  fprintf(file,"</target>\n");
  fprintf(file,"<config>\n");
  fprintf(file,"<i2nsf-security-policy xmlns=\"urn:ietf:params:xml:ns:yang:ietf-i2nsf-policy-rule-for-nsf\"\n");
  fprintf(file,"                       xmlns:nc=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n");
  fprintf(file,"  <name>%s</name>\n",policy_name);
  fprintf(file,"  <rules>\n");
  fprintf(file,"    <name>deny_ddos_attack</name>\n");
  fprintf(file,"    <condition>\n");
  fprintf(file,"      <ipv4>\n");
  fprintf(file,"        <source-ipv4-range>\n");
  fprintf(file,"          <start>%s</start>\n", str_src_start_ip_addr);
  fprintf(file,"          <end>%s</end>\n", str_src_end_ip_addr);
  fprintf(file,"        </source-ipv4-range>\n");
  fprintf(file,"      </ipv4>\n");
  fprintf(file,"    </condition>\n");
  fprintf(file,"    <action>\n");
  fprintf(file,"      <packet-action>\n");
  fprintf(file,"        <ingress-action>%s</ingress-action>\n",in_action);
  fprintf(file,"      </packet-action>\n");
  fprintf(file,"    </action>\n");
  fprintf(file,"  </rules>\n");
  fprintf(file,"</i2nsf-security-policy>\n");
  fprintf(file,"</config>\n");
  fprintf(file,"</edit-config>\n");
  fprintf(file,"</rpc>\n");
  fprintf(file,"]]>]]>\n");
  fprintf(file,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
  fprintf(file,"<rpc message-id=\"2\" xmlns=\"urn:ietf:params:xml:ns:netconf:base:1.0\">\n");
  fprintf(file,"<close-session />\n");
  fprintf(file,"</rpc>\n");
  fprintf(file,"]]>]]>\n");

  fclose (file);
  char cmd [100];
  printf("SENDING FEEDBACK TO %s\n",nsf_name);
  strcpy(cmd,"/home/ubuntu/confd-6.6/bin/netconf-console --host ");
  strcat(cmd,nsf_name);
  strcat(cmd, " /home/ubuntu/LowLevelPolicy/feedback.xml");
  system(cmd);
  rule_id++;
}

static int read_conf2(struct sockaddr_in *addr)
{
    FILE *fp;
    char policy_name[BUFSIZ];
    int policy_num, rule_num;
    int i, j, k;
    int rsock;
    if ((rsock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
        confd_fatal("Failed to open socket\n");

    if (cdb_connect(rsock, CDB_READ_SOCKET, (struct sockaddr*)addr,
                      sizeof (struct sockaddr_in)) < 0)
        return CONFD_ERR;
    if (cdb_start_session(rsock, CDB_RUNNING) != CONFD_OK)
        return CONFD_ERR;
    cdb_set_namespace(rsock, nsfintf__ns);

    policy_num = cdb_num_instances(rsock, "i2nsf-security-policy");

    printf("Policy Num: %d\n", policy_num);

    for (i = 0; i < policy_num; i++) {
      printf("Current Num: %d\n", i);
      cdb_pushd(rsock, "i2nsf-security-policy[%d]", i);
      cdb_get_str(rsock, &policy_name[0], BUFSIZ, "name");
      printf("Policy Name: %s\n", policy_name);
//      cdb_popd(rsock);

      rule_num = cdb_num_instances(rsock, "rules");
      printf("Rule Num: %d\n", rule_num);

      for (j = 0; j < rule_num; j++) {
        cdb_pushd(rsock, "rules[%d]", j);
        do_rule(rsock, fp, policy_name);
        cdb_popd(rsock);
      }
    }

        /*if (confd_start == 1) {
                sleep(10);
                confd_start = 0;
        }
        system("sudo /usr/bin/suricatasc -c reload-rules /var/run/suricata/suricata-command.socket");
*/
    printf("Success\n");

    fflush(stdout);

    return cdb_close(rsock);
}

/********************************************************************/

int start_confd(void)
{
    struct sockaddr_in addr;
    int subsock;
    int status;
    int spoint;

    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_family = AF_INET;
    addr.sin_port = htons(CONFD_PORT);

    confd_init("firewall", stderr, CONFD_TRACE);

    /*
     * Setup subscriptions
     */
    if ((subsock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
        confd_fatal("Failed to open socket\n");

    if (cdb_connect(subsock, CDB_SUBSCRIPTION_SOCKET, (struct sockaddr*)&addr,
                      sizeof (struct sockaddr_in)) < 0)
        confd_fatal("Failed to cdb_connect() to confd \n");

    if ((status = cdb_subscribe(subsock, 3, nsfreg__ns, &spoint, "/nsf-registrations"))
        != CONFD_OK) {
        fprintf(stderr, "Terminate: subscribe %d\n", status);
        exit(0);
    }
    if ((status = cdb_subscribe(subsock, 3, nsfintf__ns, &spoint, "/i2nsf-security-policy"))
        != CONFD_OK) {
        fprintf(stderr, "Terminate: subscribe %d\n", status);
        exit(0);
    }
    if (cdb_subscribe_done(subsock) != CONFD_OK)
        confd_fatal("cdb_subscribe_done() failed");
    printf("Subscription point = %d\n", spoint);

    /*
     * Read initial config
     */
    if ((status = read_conf(&addr)) != CONFD_OK) {
        fprintf(stderr, "Terminate: read_conf %d\n", status);
        exit(0);
    }
    if ((status = read_conf2(&addr)) != CONFD_OK) {
      fprintf(stderr, "Terminate: read_conf %d\n", status);
      exit(0);
    }
    /* This is the place to HUP the daemon */

    while (1) {
        static int poll_fail_counter=0;
        struct pollfd set[1];

        set[0].fd = subsock;
        set[0].events = POLLIN;
        set[0].revents = 0;

        if (poll(&set[0], 1, -1) < 0) {
            perror("Poll failed:");
            if(++poll_fail_counter < 10)
                continue;
            fprintf(stderr, "Too many poll failures, terminating\n");
            exit(1);
        }

        poll_fail_counter = 0;
        if (set[0].revents & POLLIN) {
            int sub_points[1];
            int reslen;

            printf("Reslen: %d\n",reslen);
            if ((status = cdb_read_subscription_socket(subsock,
                                                       &sub_points[0],
                                                       &reslen)) != CONFD_OK) {
                fprintf(stderr, "terminate sub_read: %d\n", status);
                exit(1);
            }
            if (reslen > 0) {
                if (((status = read_conf(&addr)) != CONFD_OK) || ((status = read_conf2(&addr)) != CONFD_OK)) {
                    fprintf(stderr, "Terminate: read_conf %d\n", status);
                    exit(1);
                }
            }

            fprintf(stderr, "Read new config, updating dhcpd config \n");
            /* this is the place to HUP the daemon */

            if ((status = cdb_sync_subscription_socket(subsock,
                                                       CDB_DONE_PRIORITY))
                != CONFD_OK) {
                fprintf(stderr, "failed to sync subscription: %d\n", status);
                exit(1);
            }
        }
    }
}

/********************************************************************/

