/*********************************************************************
 * ConfD Subscriber intro example
 * Implements a DHCP server adapter
 *
 * (C) 2005-2007 Tail-f Systems
 * Permission to use this code as a starting point hereby granted
 *
 * See the README file for more information
 ********************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>

#include <confd_lib.h>
#include <confd_cdb.h>

/* include generated file */
#include "ietf-i2nsf-policy-rule-for-nsf.h"
#include "ietf-i2nsf-feedback-policy.h"

/********************************************************************/



static int main(struct sockaddr_in *addr)
{
    FILE *fp;
    char policy_name[BUFSIZ];
    int policy_num, rule_num;
    int i, j, k;
    int rsock;

    if ((rsock = socket(PF_INET, SOCK_STREAM, 0)) < 0 )
        confd_fatal("Failed to open socket\n");

    if (cdb_connect(rsock, CDB_READ_SOCKET, (struct sockaddr*)addr,
                      sizeof (struct sockaddr_in)) < 0)
        return CONFD_ERR;
    if (cdb_start_session(rsock, CDB_RUNNING) != CONFD_OK)
        return CONFD_ERR;
    cdb_set_namespace(rsock, nsffbck__ns);

    
    policy_num = cdb_num_instances(rsock, "i2nsf-security-policy");

    printf("NSF Num: %d\n", policy_num);

    for (j = 0; j < policy_num; j++) {
      printf("Current Num: %d\n", j);
      cdb_pushd(rsock, "i2nsf-security-policy[%d]", i);
      cdb_get_str(rsock, &policy_name[0], BUFSIZ, "name");
    }

        /*if (confd_start == 1) {
                sleep(10);
                confd_start = 0;
        }
        system("sudo /usr/bin/suricatasc -c reload-rules /var/run/suricata/suricata-command.socket");
*/
    printf("Success\n");

    return cdb_close(rsock);
}
