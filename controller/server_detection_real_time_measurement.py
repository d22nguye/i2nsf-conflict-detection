#!/usr/bin/python
# -*- coding:utf-8 -*-
from MeasurementModule import module
import random
import glob
import API.CFGAPI as CFG
import API.DFAAPI as DFA
import API.converter as converter
from MeasurementModule import drawGraph, checkTheSameResult
from MeasurementModule.policy_generation import PolicyGeneration, NUMBER_SOURCE_TARGET
from checker import Checker

# Start measure RAM -----------------------------------
module.setup_ram_mesurement()
# -----------------------------------------------------

# construct DFA based Data Extractor
consumer = DFA.dfa_construction('DataModel/cfi_dm.txt')
consumer_dfa = consumer[0]
consumer_extractedinfo = consumer[1]

# construct CFG based Policy Generator
nsf_facing = CFG.cfg_construction('DataModel/nfi_dm.txt')
cfglist = nsf_facing[0]
nsf_requiredinfo = nsf_facing[1]

# construct Data Converter
dataconverter = converter.DataConverter(consumer_extractedinfo, nsf_requiredinfo)
dataconverter.initializeDB()

# construct conflicting checker
conflict_rules = []
rule_number = 100
checker = Checker([], consumer_dfa, consumer_extractedinfo, is_measure=True)


def deploy_intent(policy_file_name):
    # extract data
    consumer_extractedlist = DFA.extracting_data(policy_file_name, consumer_dfa, consumer_extractedinfo)
    dataconverter.inputExtractedData(consumer_extractedlist)

    # check conflict ----------------------------------
    checker.checkConflict(consumer_extractedlist)
    # -------------------------------------------------

    # save new rule extracted
    checker.insert_new_rule(dataconverter)
    # -------------------------------------------------

    # convert data & provisioning
    print('Convert data...')
    dataconverter.convertData()
    print('Policy provisioning...')


if __name__ == "__main__":
    # randomly generate rule set ----------------------
    actual_number_rule = 0
    actual_size_pass = 0
    actual_size_explicit = 0
    actual_size_implicit = 0
    rule_generation_instance = PolicyGeneration()
    is_generate_new_set = True
    if is_generate_new_set:
        while not ((actual_number_rule == rule_number) & ((actual_size_pass * 1.0 / rule_number) < 0.52) & (
                (actual_size_pass * 1.0 / rule_number) > 0.498)):
            conflict_rules, never_conflicting_rules, invalid_partial_ordering, actual_number_rule, actual_size_explicit, actual_size_implicit, actual_size_pass, size_drop = \
                rule_generation_instance.initialize_high_level_policy_DB(rule_number=rule_number,
                                                                         is_contain_implicit=True,
                                                                         partial_ordering_number=10,
                                                                         max_num_conflict=1000,
                                                                         percent_explicit=0.5, conflict_percentage=1)
    rule_generation_instance.add_converted_data()
    # -------------------------------------------------

    # deploy separation constraint --------------------
    max_deploy_separation_number = 1000
    index_deployed_separation = 0
    is_delpoy_separation_constraint = True
    invalid_constraint = []
    if is_delpoy_separation_constraint:
        file_separation = sorted(glob.glob("HighLevelPolicy/Usecase/separation*.txt"))

        for file in file_separation:
            if not ((index_deployed_separation < max_deploy_separation_number) or (
                    index_deployed_separation < rule_generation_instance.index_separation)):
                break
            index_deployed_separation += 1
            checker.SC_API.deploy_separation_constraint(file)
    # -------------------------------------------------

    # deploy partial ordering relationship ------------
    is_delpoy_order_relationship = False
    invalid_ordering = []
    if is_delpoy_order_relationship:
        file_ordering = sorted(glob.glob("HighLevelPolicy/Usecase/ordering*.txt"))
        for file in file_ordering:
            rs = checker.PO_API.deploy_partial_ordering_relationship(file)
            if rs:
                invalid_ordering.append(rs)
    # -------------------------------------------------

    # deploy rules ------------------------------------
    file_rule = sorted(glob.glob("HighLevelPolicy/Usecase/R*.txt"))
    random.shuffle(file_rule)
    k = 0
    for rule in file_rule:
        deploy_intent(rule)
        print(str(k) + "th deployed")
        k += 1
    # -------------------------------------------------

    # Checking accuracy of the algorithm --------------
    module.end_process = 1
    if is_generate_new_set:
        isSame = checkTheSameResult.check_same_contents_with_constraint_list(checker.total_conflicting_rules,
                                                                             conflict_rules, never_conflicting_rules)
        if isSame:
            module.print_rs(100, "Same result")
        else:
            module.print_rs(100, "Different")

        isDifferent = checkTheSameResult.check_accuracy_partial_ordering_detect(invalid_partial_ordering,
                                                                                invalid_ordering)
        if isDifferent:
            module.print_rs(100, "Wrong invalid ordering relationship")
        else:
            module.print_rs(100, "Correct ordering relationship")
    # -------------------------------------------------

    # Store evaluation data ---------------------------
    info_set = []
    execution_time_range = []
    for exec_time in checker.list_execution_time:
        if len(execution_time_range) < len(checker.list_execution_time):
            execution_time_range.append(exec_time)
    info_set = [len(checker.total_conflicting_rules), actual_size_explicit, actual_size_implicit]

    drawGraph.save_execution_time(execution_time_range, 2, rule_number)
    drawGraph.save_ram_usage(2, rule_number)
    drawGraph.save_exec_time_total(sum(execution_time_range), 2, rule_number)
    drawGraph.save_ram_usage_total(2, rule_number)

    print 'total execution time (only detection): ', sum(execution_time_range), ' - average total execution time: ', sum(
        execution_time_range) / len(execution_time_range), ' - conflicts:', info_set[0], ' - explicit:', info_set[
        1], ' - implicit:', info_set[
        2], ' - rule number:', actual_number_rule, ' - separation number:', rule_generation_instance.index_separation
    # -------------------------------------------------
