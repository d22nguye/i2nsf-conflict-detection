# I2NSF-conflict-detection

Detection and resolution mechanisms are intergrated into Security Controller of I2NSF with the referred public implementation [i2nsf-framework](https://github.com/jaehoonpaul/i2nsf-framework). At the consumer-facing interface, we introduce the pair-wise rules checking procedure, which is employed to detect conflicts, and the use of separation constraints (SC) and partial ordering relationships (POV) helps in preventing conflicts

## The intergration will include 4 new files stand for 4 main APIs respectively:

- [ ] *highLevelDetectionAPI.py*: pair-wise conflicting rule checking API
- [ ] *partialOrderingRelationshipAPI.py*: handle partial ordering relationship deployment and validation checker
- [ ] *separationConstraintAPI.py*: handle separation constraint deployment and validation checker
- [ ] *checker.py*: pair-wise rules checker to detect potential conflicting rules

## Getting started

- [ ] Ensure that MySQL is installed, and run the following command to initiate the database used for I2NSF translation:


```
./initializeDB.sh
```

## Run tools

There are two main options for running the test on our mechanism:

- [ ] Run *server_detection_real_time.py* which can be considered as real-time checking use case (Including auto rule generation, rule deployment, SC and POR specification, and detection)
- [ ] *server_detection_real_time_measurement.py* is another version of the test case to evaluate the accuracy and measure execution time as well as RAM usage

## Supporting files

- [ ] We add a folder *MeasurementModule* which contains the supporting files used for evaluation purpose: rule generation, accuracy check, graph drawer for evaluation result output
- [ ] Run *MeasurementModule/drawGraph.py* to see the result of our evaluation shown in the paper
